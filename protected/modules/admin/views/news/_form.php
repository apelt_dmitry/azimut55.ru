<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>
    

    <?= $form->fileFieldControlGroup($model,'image'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlenght'=>32)); ?>
    <div class="form-group">
        <?= $form->labelEx($model, 'description', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'description',
                'defaultValue' => $model->description,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>  
    <div class="form-group">
        <?= $form->labelEx($model, 'date', array('class'=>'control-label col-lg-2')); ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.jui.EJuiDateTimePicker',array(
                'model'=>$model,
                'attribute'=>'date',
                'mode'=>'date',
                'options'=>array(
                    'showAnim'=>'fold',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'showButtonPanel'=> true,
                    'dateFormat'=>'dd.mm.yy',
                    //'timeFormat'=>'hh:mm',
                ),
                'language'=>'ru',
                'htmlOptions'=>array(
                    'class'=>'form-control',
                ),
            )); ?>
            <?= $form->error($model, 'date') ?>
        </div>
    </div>

    <?= BsHtml::formActions(array(
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>




<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?php
$this->endWidget();
?>

