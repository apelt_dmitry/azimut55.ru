<?php echo BsHtml::pageHeader('Интересное') ?>




<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'header' => 'preview',
            'type'=>'raw',
            'value' => 'BsHtml::image("/uploads/interesting/preview/".$data->preview)',
        ),
        'title',
        array(
            'name'=>'description',
            'type'=>'raw',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}',
            
        ),
    ),
)); ?>