<?php

Class DefaultController extends AdminController
{

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }


    public function actionIndex()
    {
        //$this->render('index');
        $this->redirect(array('/admin/main/index'));
    }

    public function actionBanner(){
        $model = Config::model()->findByPk(1);
        if ( isset($_POST['Config']) ) {
            $model->attributes = $_POST['Config'];
            if($model->save()){
                Yii::app()->user->setFlash('success', true);
            };
        }
        $this->render('banner', array('model'=>$model));
    }

    public function actionLogin()
    {
        $model = new User('login');

        if ( isset($_POST['User']) ) {
            $model->attributes = $_POST['User'];
            if ( $model->validate() && $model->login() ) {
                $this->redirect(array('index'));
            }
        }

        $this->render('login', array(
            'model'=>$model,
        ));
        //echo CPasswordHelper::hashPassword('admin228');
    }
    
    
}