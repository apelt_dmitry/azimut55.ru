<?php

class ApietraController extends AdminController
{

    
//    public function actionCreate()
//    {
//        $model = new Apietra();
//
//        if ( isset($_POST['Apietra']) ) {
//            $model->attributes = $_POST['Apietra'];
//            if( $model->save() ) {
//                $this->redirect(array('index'));
//            }
//        }
//
//        $this->render('create', array(
//            'model'=>$model,
//        ));
//    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Apietra']) ) {
            $model->attributes = $_POST['Apietra'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }   
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


//    public function actionDelete($id)
//    {
//        if ( $id==1 ) {
//            Yii::app()->end();
//        }
//        $this->loadModel($id)->delete();
//
//        if ( !isset($_GET['ajax']) ) {
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//        }
//    }


    public function actionIndex()
    {
        $model = new Apietra('search');
        $model->unsetAttributes();
        if ( isset($_GET['Apietra']) ) {
            $model->attributes=$_GET['Apietra'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Apietra::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
