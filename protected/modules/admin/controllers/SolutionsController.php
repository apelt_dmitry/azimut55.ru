<?php

class SolutionsController extends AdminController
{

    
//    public function actionCreate()
//    {
//        $model = new Solutions();
//
//        if ( isset($_POST['Solutions']) ) {
//            $model->attributes = $_POST['Solutions'];
//            if( $model->save() ) {
//                $this->redirect(array('index'));
//            }
//        }
//
//        $this->render('create', array(
//            'model'=>$model,
//        ));
//    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Solutions']) ) {
            $model->attributes = $_POST['Solutions'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }   
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


//    public function actionDelete($id)
//    {
//        if ( $id==1 ) {
//            Yii::app()->end();
//        }
//        $this->loadModel($id)->delete();
//
//        if ( !isset($_GET['ajax']) ) {
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//        }
//    }


    public function actionIndex()
    {
        $model = new Solutions('search');
        $model->unsetAttributes();
        if ( isset($_GET['Solutions']) ) {
            $model->attributes=$_GET['Solutions'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Solutions::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
