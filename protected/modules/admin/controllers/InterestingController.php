<?php

class InterestingController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Interesting();
        
        if ( isset($_POST['Interesting']) ) {
            $model->attributes = $_POST['Interesting'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/interesting/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/interesting/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){
                    
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        if ( isset($_POST['Interesting']) ) {
            $model->attributes = $_POST['Interesting'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/interesting/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/interesting/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Interesting('search');
        $model->unsetAttributes();
        if ( isset($_GET['Interesting']) ) {
            $model->attributes=$_GET['Interesting'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Interesting::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
