<?php

Class ConfigController extends AdminController
{

    public function actionIndex()
    {
        $model = $this->config;
        
        if( isset($_POST['Config']) ) {
            $model->attributes = $_POST['Config'];
            
            if ( $model->save() ) {
                Yii::app()->user->setFlash('success', true);
                $this->redirect(array('config/index'));
            }
        }
        
        $this->render('index', array(
            'model'=>$model,
        ));
    }
    
}