<?php

class NewsController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new News();
        
        if ( isset($_POST['News']) ) {
            $model->attributes = $_POST['News'];
            $model->date = strtotime($model->date);
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/news/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/news/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){
                    
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $model->date=date('d.m.Y',$model->date);
        if ( isset($_POST['News']) ) {
            $model->attributes = $_POST['News'];
            $model->date = strtotime($model->date);
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/news/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/news/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new News('search');
        $model->unsetAttributes();
        if ( isset($_GET['News']) ) {
            $model->attributes=$_GET['News'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=News::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
