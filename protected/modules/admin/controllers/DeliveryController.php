<?php

class DeliveryController extends AdminController
{

    
//    public function actionCreate()
//    {
//        $model = new Delivery();
//
//        if ( isset($_POST['Delivery']) ) {
//            $model->attributes = $_POST['Delivery'];
//            if( $model->save() ) {
//                $this->redirect(array('index'));
//            }
//        }
//
//        $this->render('create', array(
//            'model'=>$model,
//        ));
//    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Delivery']) ) {
            $model->attributes = $_POST['Delivery'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }   
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


//    public function actionDelete($id)
//    {
//        if ( $id==1 ) {
//            Yii::app()->end();
//        }
//        $this->loadModel($id)->delete();
//
//        if ( !isset($_GET['ajax']) ) {
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//        }
//    }


    public function actionIndex()
    {
        $model = new Delivery('search');
        $model->unsetAttributes();
        if ( isset($_GET['Delivery']) ) {
            $model->attributes=$_GET['Delivery'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Delivery::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
