<?php

class OrderForm extends CFormModel 
{
    public $type;
    public $name;
    public $phone1;
    public $phone2;
    public $email;
    public $message;
    public $company_name;
    public $adress;
    public $website;
    public $director;
    public $ogrn;
    public $inn;
    public $kpp;
    public $bank;
    public $bik;
    public $bank_account;
    public $korr_account;
    public $agreement;


    public function rules()
    {
        return array(
            array('name, korr_account, bank_account, ogrn, inn, kpp, bik, agreement, type, phone1, email, message','required'),
            array('name,  email', 'length', 'max'=>64),
            array('message', 'length', 'max'=>1024),
            array('email','email'),
            array('phone1, phone2', 'length', 'max'=>16),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
            array('services','safe'),
            array(
                'company_name', 'length', 'max'=>64,
                'message'=>'Введите название компании'
                ),
            
            );
        
        
    }


    public function attributeLabels()
    {
        return array(
            'type'=>'Тип',
            'name'=>'Контактное лицо',
            'phone1'=>'Телефон',
            'phone2'=>'Доп. Телефон',
            'email'=>'E-mail',
            'adress'=>'Адрес',
            'website'=>'Сайт',
            'director'=>'Директор',
            'ogrn'=>'ОГРН',
            'inn'=>'ИНН',
            'kpp'=>'КПП',
            'bank'=>'Банк',
            'bank_account'=>'Расчётный счёт',
            'bik'=>'Бик',
            'korr_account'=>'Корр. счёт',
            'message'=>'Текст сообщения',
            'company_name'=>'Название компании',
            'agreement'=>'Я соглашаюсь с условиями <a style="color:#008cba;" href="/site/agreement" target="_blank">пользовательского соглашения</a>',
        );
    }
    
    public function goOrder()
    {
        include_once "libmail.php";
        $m = new Mail;
        $m->From($this->email.';admin@azimut55.ru');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $m->Body(''
                . 'Тип: '.$this->type.'. '
                . 'Отправитель: '.$this->name.'. '
                . 'Телефон: '.$this->phone1. '. '
                . 'Доп. Телефон: '.$this->phone2. '. '
                . 'E-mail: '.$this->email. '. '
                . 'Адрес: '.$this->adress. '. '
                . 'Сайт: '.$this->website. '. '
                . 'Директор: '.$this->director. '. '
                . 'ОГРН: '.$this->ogrn. '. '
                . 'ИНН: '.$this->inn. '. '
                . 'КПП: '.$this->kpp. '. '
                . 'Банк: '.$this->bank. '. '
                . 'Бик: '.$this->bik. '. '
                . 'Расчётный счёт: '.$this->bank_account. '. '
                . 'Корр. счёт: '.$this->korr_account. '. '
                . 'Название компании: '.$this->company_name. '.'
                );
        $m->Send();
    }
    
    
}
