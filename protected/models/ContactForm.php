<?php

class ContactForm extends CFormModel
{
    public $name;
    public $phone;
    public $email;
    public $message;
 
    public function rules()
    {
        return array(
            array('name, phone, email, message','required'),
            array('name, email', 'length', 'max'=>64),
            array('message', 'length', 'max'=>1024),
            array('email','email'),
            array('phone','length', 'max'=>16),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
        );
    }


    public function attributeLabels()
    {
        return array(
            'name'=>'Контактное лицо',
            'phone'=>'Телефон',
            'email'=>'E-mail',
            'message'=>'Текст сообщения'
        );
    }
    
    public function goContact()
    {
        include_once "libmail.php";
        $m = new Mail;
        $m->From($this->email.';admin@azimut55.ru');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $m->Body('Отправитель: '.$this->name.'. Телефон: '.$this->phone. '. Сообщение: '.$this->message);
        $m->Send();
    }
}
