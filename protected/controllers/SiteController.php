<?php

class SiteController extends Controller
{

	public function actionIndex()
	{
            $model = Main::model()->findByPk(1);
            $this->pageTitle = 'Главная';
            $this->render('index', array(
                'model' => $model,
            ));

	}
        
        public function actionStone() 
        {
            $model = Stone::model()->findByPk(1);
            $this->pageTitle = 'Искусственный камень';
            $this->render('stone', array(
                'model' => $model,
            ));
        }
        
        public function actionGlue() 
        {
            $model = Glue::model()->findByPk(1);
            $this->pageTitle = 'Клей для искусственного камня';
            $this->render('glue', array(
                'model' => $model,
            ));
        }
        
        public function actionTools() 
        {
            $model = Tools::model()->findByPk(1);
            $this->pageTitle = 'Инструменты';
            $this->render('tools', array(
                'model' => $model,
            ));
        }
        
        public function actionDelivery() 
        {
            $model = Delivery::model()->findByPk(1);
            $this->pageTitle = 'Доставка';
            $this->render('delivery', array(
                'model' => $model,
            ));
        }
        
        public function actionAgreement() 
        {
            $this->render('agreement');
        }

        public function actionPrice() 
        {
            $model = Price::model()->findByPk(1);
            $this->pageTitle = 'Цены';
            $this->render('price', array(
                'model' => $model,
            ));
        }
       
        
        public function actionSolutions()
        {
            $model = Solutions::model()->findByPk(1);
            $this->pageTitle = 'Готовые решения';
            $this->render('solutions', array(
                'model' => $model,
            ));
        }
        
        public function actionContacts() 
        {
            $model = Contacts::model()->findByPk(1);
            $this->pageTitle = 'Контакты';
            $this->render('contacts', array(
                'model' => $model,
            ));
        }
        
        public function actionNews()
	{
            $criteria = new CDbCriteria();
            $criteria->order = '`id` DESC';
            $model = News::model()->findAll($criteria);
            $this->pageTitle = 'Новости';
            $this->render('news', array(
                'model' => $model,
            ));
	}
        
        public function actionInteresting()
	{
            $criteria = new CDbCriteria();
            $criteria->order = '`id` DESC';
            $model = Interesting::model()->findAll($criteria);
            
            $this->render('news', array(
                'model' => $model,
            ));
	}
        
        public function actionAkrilika()
	{
            $model = Akrilika::model()->findByPk(1);
            $this->pageTitle = 'Akrilika';
            $this->render('series_akrilika', array(
                'model' => $model,
            ));
        }
        public function actionApietra()
	{
            $model = Apietra::model()->findByPk(1);
            $this->pageTitle = 'Apietra';
            $this->render('series_apietra', array(
                'model' => $model,
            ));
        }
        public function actionDesign()
	{
            $model = Design::model()->findByPk(1);
            $this->pageTitle = 'Design';
            $this->render('series_design', array(
                'model' => $model,
            ));
        }
        public function actionKristall()
	{
            $model = Kristall::model()->findByPk(1);
            $this->pageTitle = 'Kristall';
            $this->render('series_kristall', array(
                'model' => $model,
            ));
        }
        
       
        
        public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
        
        public function actionContact()
        {
            $model = new ContactForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='contact-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['ContactForm']) ) {
                $model->attributes = $_POST['ContactForm'];
                if ( $model->validate() ) {
                    $model->goContact();
                    echo '1';
                } else {
                    echo '0';
                }
                Yii::app()->end();
            }
            $this->redirect(array('site/index'));
        }
        
        public function actionOrder()
        {
            $model = new OrderForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='order-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['OrderForm']) ) {
                $model->attributes = $_POST['OrderForm'];
                if ( $model->validate() ) {
                    $model->goOrder();
                    echo '1';
                } else {
                    echo '0';
                    print_r($model->errors);
                }
                Yii::app()->end();
            }
            $this->redirect(array('site/index'));
        }
}