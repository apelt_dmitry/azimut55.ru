<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<div class="row">
    <?php foreach ($model as $row):?>
    <div class="col-md-12" id="news<?= $row->id ?>">
        <h1><?= $row->title ?></h1>
        <br>
    </div>
    <div class="category-info">
        <div class="col-md-3">
            <div class="image thumbnail">
                
                <a class="gallery" href="/uploads/news/<?= $row->preview?>"><img class="gallery" src="/uploads/news/<?= $row->preview?>" alt="IMG"></a>
            </div>
        </div>
        <div class="col-md-9 news_background">
            <p><?= $row->description ?></p>
        </div>
        <div class="news_date"><?= Yii::app()->dateFormatter->format('d MMM yyyy г.', $row->date) ?></div>
    </div>
    <?php endforeach; ?>
</div>