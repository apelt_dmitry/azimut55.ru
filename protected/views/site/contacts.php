<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<div class="col-md-9 hidden-xs">
    <div>Адрес: г.Омск ул.Герцена, 268а, офис 306</div><br/>
    <div>Телефон №1: 8-906-197-77-79</div><br/>
    <div>Телефон №2: 8-900-670-33-44</div><br/>
    <div>Email: <?= BsHtml::link('azimutomsk@mail.ru', 'mailto:azimutomsk@mail.ru', array('target'=>'_blank')) ?></div><br/>

</div>
<div class="col-md-3 hidden-xs">
    <div id="column-right">
        <div class="portlet interest">
            <div class="portlet-decoration">
                <div class="portlet-title"><span>Последние новости</span></div>
            </div>
            <div class="portlet-content">
                <?php $this->renderPartial('application.components.views.news'); ?>
            </div>
        </div>
    </div>
</div>
</div>