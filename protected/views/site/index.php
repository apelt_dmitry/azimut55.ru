<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<script type="text/javascript">
        $(document).ready(function() {
            $("#responsive").lightSlider({
                autoWidth: true,
                loop:true,
                auto:true,
            }); 
        });
    </script>
    <ul id="responsive">
        <li>
            <img src="/img/slide/1.jpg">
        </li>
        <li>
            <img src="/img/slide/2.jpg">
        </li>
        <li>
            <img src="/img/slide/3.jpg">
        </li>
        <li>
            <img src="/img/slide/4.jpg">
        </li>
        <li>
            <img src="/img/slide/5.jpg">
        </li>
        <li>
            <img src="/img/slide/6.jpg">
        </li>
        <li>
            <img src="/img/slide/7.jpg">
        </li>
        <li>
            <img src="/img/slide/8.jpg">
        </li>
        <li>
            <img src="/img/slide/9.jpg">
        </li>
        <li>
            <img src="/img/slide/10.jpg">
        </li>
        <li>
            <img src="/img/slide/11.jpg">
        </li>
    </ul>   
<div class="col-md-3 hidden-xs">
    <div id="column-right">
        <div class="portlet interest">
            <div class="portlet-decoration">
                <div class="portlet-title"><span>Интересное</span></div>
            </div>
            <div class="portlet-content">
                <?php $this->renderPartial('application.components.views.interesting'); ?>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    
    <!--<h1><?= $model->name ?></h1>-->
    <div id="page">
        <?= $model->body ?>
    </div>
</div>
<div class="col-md-3 hidden-xs">
    <div id="column-right">
        <div class="portlet interest">
            <div class="portlet-decoration">
                <div class="portlet-title"><span>Последние новости</span></div>
            </div>
            <div class="portlet-content">
                <?php $this->renderPartial('application.components.views.news'); ?>
            </div>
        </div>
    </div>
</div>
</div>

