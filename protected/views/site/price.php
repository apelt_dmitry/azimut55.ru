<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<div class="col-md-3 hidden-xs">
    <div id="column-right">
        <div class="portlet interest">
            <div class="portlet-decoration">
                <div class="portlet-title"><span>Интересное</span></div>
            </div>
            <div class="portlet-content">
                <?php $this->renderPartial('application.components.views.interesting'); ?>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <h1><?= $model->name ?></h1>
    <div id="page">
        <?= $model->body ?>
    </div>
</div>
<div class="col-md-3 hidden-xs">
    <div id="column-right">
        <div class="portlet interest">
            <div class="portlet-decoration">
                <div class="portlet-title"><span>Последние новости</span></div>
            </div>
            <div class="portlet-content">
                <?php $this->renderPartial('application.components.views.news'); ?>
            </div>
        </div>
    </div>
</div>
</div>