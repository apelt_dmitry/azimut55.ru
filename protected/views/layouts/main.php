<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    //preLoader
    ->registerCssFile($pt.'css/preLoader.css')
    //lightSlider
    ->registerCssFile($pt.'css/lightSlider.css')
    //template
    ->registerCssFile($pt.'css/template/carousel.css')
    ->registerCssFile($pt.'css/template/jquery-ui-1.8.16.custom.css')
    ->registerCssFile($pt.'css/template/slideshow.css')
    ->registerCssFile($pt.'css/template/stylesheet.css')
    ->registerCssFile($pt.'css/template/theme.css')
    ->registerCssFile($pt.'css/template/yeti.css')
       
    ->registerCssFile($pt.'css/main.css');

$cs
    
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/jquery-1.8.2.min.js')
    //->registerScriptFile($pt.'js/ddl/jquery-1.11.1.min.js')
    //template
    
//    ->registerScriptFile($pt.'js/template/common.js',CClientScript::POS_END)
//    ->registerScriptFile($pt.'js/template/jquery-1.7.1.min.js',CClientScript::POS_END)
//    ->registerScriptFile($pt.'js/template/jquery-ui-1.8.16.custom.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/template/jquery.nivo.slider.pack.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/template/jquery.jcarousel.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/lightslider/jquery.lightSlider.js',CClientScript::POS_END)
//    ->registerScriptFile($pt.'js/template/bootstrap.min.js',CClientScript::POS_END)
        
    ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END);

    $criteria = new CDbCriteria();
    $criteria->select = array('id', 'preview', 'title', 'description', 'date');
    $criteria->order = '`id` DESC';
    $criteria->limit =3;
    $lastNews = News::model()->findAll($criteria);
    
    $config=$this->config;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="<?= $config->meta_keys ?>">
        <meta name="title" content="<?= $config->title ?>">
        <meta name="description" content="<?= $config->meta_description ?>">
        <title><?php echo $this->pageTitle; ?></title>
        <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
    </head>
    <body>
        <script>
            $(document).ready(function () {
                $('#loader-wrapper').animate({
                    opacity: 0
                }, 1000, null, function () {
                    $(this).hide();
                });
            });
            
        </script>
        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>
        <div class="container">
            <div class="row" id="header"><!-- start header -->
                
                <div class="col-md-3 logo">
                    <a href="/">
                        <img src="/img/logotip.png" title="Azimut" alt="Azimut">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="header-mid">Азимут - официальный дилер 
                        <a href="http://akrilika.com/">
                            <img src="/img/logo_bg.png" title="akrilika" alt="akrilika">
                        </a> 
                        <br/>в Казахстане</div>
                    </div>
                <div class="col-md-3">
                    <script type="text/javascript">
                        (function() {
                        if (window.pluso)if (typeof window.pluso.start == "function") return;
                        if (window.ifpluso==undefined) { window.ifpluso = 1;
                            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                            var h=d[g]('body')[0];
                            h.appendChild(s);
                        }})();
                    </script>
                    <div class="phone">Телефон: 8-906-197-77-79</div>
                    <div class="phone">Телефон: 8-900-670-33-44</div>
                    <div class="email">Email:<?= BsHtml::link('azimutomsk@mail.ru', 'mailto:azimutomsk@mail.ru', array('target'=>'_blank')) ?></div><br/>
                    <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
                
                    <br/>
                    <a class="footer_text2 footer_text3" href="#" onclick="$('#contactDialog').dialog('open'); return false;"></a>
                </div>
                <!--Начало формы отправки письма-->
                    <?php
                        $contactForm = new ContactForm;
                    ?>
                    <?php
                        $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                            'id'=>'contactDialog',
                            // additional javascript options for the dialog plugin
                            'options'=>array(
                                'draggable'=>false,
                                'resizable'=>false,
                                'modal'=>true,
                                'title'=>'Задать вопрос',
                                'autoOpen'=>false,
                                'width'=>'700',
                                'open'=>'js:function(){$(\'.ui-widget-overlay\').click(function(){
                                    $(\'#contactDialog\').dialog(\'close\');
                                })}',
                            ),
                        )); 
                    ?>
                        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                            'id'=>'contact-form',
                            'action'=>array('/site/contact'),
                            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                'validateOnChange'=>false,
                                'validateOnSubmit'=>true,
                                'afterValidate' => "js: function(form, data, hasError) {
                                    if ( !hasError) {
                                        $.ajax({
                                            type: 'POST',
                                            url: $('#contact-form').attr('action'),
                                            data: $('#contact-form').serialize(),
                                            success: function(data_inner) {
                                                if ( data_inner==1 ) {
                                                    $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                                } else {
                                                    alert('Хъюстон у нас проблемы!!!!');
                                                }
                                            }
                                        });
                                    }
                                    return false;
                                }
                                ",
                            ),
                        )); ?>

                            <div class="dialog_title">Ваш вопрос</div>
                            <?= $form->textFieldControlGroup($contactForm,'name', array(
                                'placeHolder'=>'Контактное лицо',
                            )); ?>

                            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                                'placeHolder'=>'Телефон',
                            )); ?>

                            <?= $form->textFieldControlGroup($contactForm,'email', array(
                                'placeHolder'=>'E-mail',
                            )); ?>

                            <?= $form->textAreaControlGroup($contactForm,'message', array(
                                //'placeHolder'=>'Ваше сообщение',
                                'resize'=>'false',
                            )); ?>

                            <?=BsHtml::submitButton('Отправить вопрос', array(
                                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                                'icon' => BsHtml::GLYPHICON_ENVELOPE,
                                'class' => 'float_right',
                            ))?>

                        <?php $this->endWidget(); ?>
                    <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
                            <!--Конец формы отправки письма-->
                            
                            
                            <!--Начало формы заказа-->
                    <?php
                        $orderForm = new OrderForm;
                    ?>
                    <?php
                        $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                            'id'=>'orderDialog',
                            // additional javascript options for the dialog plugin
                            'options'=>array(
                                'draggable'=>false,
                                'resizable'=>false,
                                'modal'=>true,
                                'title'=>'Сделать заказ',
                                'autoOpen'=>false,
                                'width'=>'700',
                                'open'=>'js:function(){$(\'.ui-widget-overlay\').click(function(){
                                    $(\'#orderDialog\').dialog(\'close\');
                                })}',
                            ),
                        )); 
                    ?>
                        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                            'id'=>'order-form',
                            'action'=>array('/site/order'),
                            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                'validateOnChange'=>false,
                                'validateOnSubmit'=>true,
                                'afterValidate' => "js: function(form, data, hasError) {
                                    if ( !hasError) {
                                        $.ajax({
                                            type: 'POST',
                                            url: $('#order-form').attr('action'),
                                            data: $('#order-form').serialize(),
                                            success: function(data_inner) {
                                                if ( data_inner==1 ) {
                                                    $('#order-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                                } else {
                                                    alert('Хъюстон у нас проблемы!!!!');
                                                }
                                            }
                                        });
                                    }
                                    return false;
                                }
                                ",
                            ),
                        )); ?>

                            <div class="dialog_title">Ваш заказ</div>
                            
                            <?php echo $form->dropDownListControlGroup($orderForm,'type', array('Юридическое лицо'), array(
                                //'empty' => 'no',
                                //'help'=>'Для создания подпунктов - выберите родителя.',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'name', array(
                                'placeHolder'=>'Контактное лицо',
                            )); ?>
                            

                            <?= $form->textFieldControlGroup($orderForm,'phone1', array(
                                'placeHolder'=>'Телефон',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'phone2', array(
                                'placeHolder'=>'Доп. Телефон',
                            )); ?>
                            

                            <?= $form->textFieldControlGroup($orderForm,'email', array(
                                'placeHolder'=>'E-mail',
                            )); ?>

                            
                            <?= $form->textAreaControlGroup($orderForm,'message', array(
                                //'placeHolder'=>'Ваше сообщение',
                                'resize'=>'false',
                            )); ?>
                           
                            
                            <?= $form->textFieldControlGroup($orderForm,'adress', array(
                                'placeHolder'=>'Адрес',
                            )); ?>
                            
                            <?= $form->textFieldControlGroup($orderForm,'company_name'); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'website', array(
                                'placeHolder'=>'Сайт',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'director', array(
                                'placeHolder'=>'Директор',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'ogrn', array(
                                'placeHolder'=>'ОГРН',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'inn', array(
                                'placeHolder'=>'ИНН',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'kpp', array(
                                'placeHolder'=>'КПП',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'bank', array(
                                'placeHolder'=>'Банк',
                            )); ?>
                            
                            <?= $form->textFieldControlGroup($orderForm,'bank_account', array(
                                'placeHolder'=>'Расчётный счёт',
                            )); ?>
                            
                            
                            <?= $form->textFieldControlGroup($orderForm,'bik', array(
                                'placeHolder'=>'Бик',
                            )); ?>
                            
                            

                            <?= $form->textFieldControlGroup($orderForm,'korr_account', array(
                                'placeHolder'=>'Корр. счёт',
                            )); ?>
                            
                            <?= $form->checkBoxControlGroup($orderForm,'agreement', array(
                                //'placeHolder'=>'Пользовательское соглашение',
                                'labelOptions'=>array('class'=>'col-lg-12')
                            )); ?>
                            
                                                      
                            <?=BsHtml::submitButton('Оформить заказ', array(
                                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                                'icon' => BsHtml::GLYPHICON_ENVELOPE,
                                'class' => 'float_right',
                            ))?>

<!--                            <div class="dialog_footer">* Пожалуйста заполните все белые поля: Контактное лицо ;  Телефон; E-mail;</div>-->

                        <?php $this->endWidget(); ?>
                    <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
                            <!--Конец формы заказа-->
                            
            </div><!-- end header -->
            
            
            <div class="row"><!-- start nav -->
                <div class="col-md-12">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="">
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                            $this->widget('zii.widgets.CMenu', array(
                                                'id'=>'main_menu',
                                                'items'=>array(
                                                    array('label'=>'Главная', 'url'=>array('site/index'),'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Услуги', 'url'=>array('site/stone'),'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Фото объектов', 'url'=>array('site/tools'),'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Кованные изделия', 'url'=>array('site/delivery'),'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Документы','url'=>array('site/solutions'), 'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Новости', 'url'=>array('site/news'),'itemOptions'=> array('class'=>'li_menu')),
//                                                    array('label'=>'Всё о камне', 'url'=>array('site/price'),'itemOptions'=> array('class'=>'li_menu')),
                                                    array('label'=>'Контакты', 'url'=>array('site/contacts'),'itemOptions'=> array('class'=>'li_menu')),
                                                ),
                                                'htmlOptions'=>array(
                                                    'class'=>'nav navbar-nav',

                                                ),
                                                'activateParents'=>true,
                                                'encodeLabel'=>false,
                                            ));
                                        ?>
                                        <div class="nav navbar-nav navbar-right">
                                            <button class="btn btn-primary btn-small search_btn footer_text3" type="submit" onclick="$('#orderDialog').dialog('open'); return false;">
                                                Сделать заказ
                                            </button>
                                            <div class="divider-vertical"></div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.navbar-collapse -->
                        </div>
                    </nav>
                </div>
            </div><!-- end nav -->
            <div id="notification"></div>
            <div id="content">
                <div class="row">    
                    <?= $content ?>
                </div>
            </div>
        <footer   style="width: 940px;  margin: 0 auto;">
            <hr>
            <div class="row well no_margin_left">
                <div class="col-md-3">
                    <div class="column">
                        <div>
                            <a href="/">
                                <img style="width: 168px;" src="/img/logotip.png" title="Azimut" alt="Azimut">
                            </a>    
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        
                            <img style="width: 168px; margin-top: 30px;" src="/img/logo_bg.png" title="Akrilika" alt="Akrilika">
                         
                    </div>
                </div>
                <div class="col-xs-12 visible-xs"></div>
                <div class="col-md-3">
                    <div>
                        <a href="/site/tools">
                            <img style="width: 168px; margin-top: 30px;" src="/img/festool.jpg" title="festool" alt="festool">
                        </a>  
                    </div>
                </div>
                <div class="col-md-3">
                    <h4>Контакты</h4>
                    <ul>
                        <li><a href="#">Телефон: 8-906-197-77-79</a></li>
                        <li><a href="#">Телефон: 8-900-670-33-44</a></li>
                        <li><a href="#">Email: <?= BsHtml::link('azimutomsk@mail.ru', 'mailto:azimutomsk@mail.ru', array('target'=>'_blank')) ?></a></li>
                        <li><a href="#">Адрес: г.Омск ул.Герцена, 268а, офис 306</a></li>
                    </ul>
                </div>
            </div>
        </footer>
            <div class="row">
                <div class="col-md-8">  
                </div>
                <div class="col-md-2">
                <div class="created_by">created by</div>
                <a class="logo_footer" style="float: left; margin-top: 10px;" href="http://mir-it.info/">
                    <img style="width: 100px"src="/img/logo_footer.png" alt="">
                </a> 
                </div>
            </div>
            
        
    </div>
       

    </body>
</html>
