<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
        
    ->registerCssFile($pt.'css/bootstrap.css')
    ->registerCssFile($pt.'css/default.css')
    ->registerCssFile($pt.'css/template.css')
    ->registerCssFile($pt.'css/superfish.css')
    ->registerCssFile($pt.'css/superfish-navbar.css')
        
    ->registerCssFile($pt.'css/main.css');

$cs
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/main.js');

    $result = array(
        //array('label'=>'Главная', 'url'=>array('site/index')),
    );
    $criteria = new CDbCriteria();
    $criteria->select = array('id', 'name', 'parent_id');
    $criteria->order = '`parent_id` ASC, `z_index` ASC';
    $menu = Menu::model()->findAll($criteria);
    foreach ( $menu as $row ) {
        if ( $row->parent_id==0 ) {
            $result[$row->id] = array('label'=>$row->name, 'url'=>array('site/page', 'id'=>$row->id));
        } else {
            $result[$row->parent_id]['items'][] = array('label'=>$row->name, 'url'=>array('site/page', 'id'=>$row->id));
            $result[$row->parent_id]['itemOptions'] = array('class' => 'parent');  
        }
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="Школа Таро, обучение Таро, магия, эзотерика, руны, нумерология, гадание, гадания на Таро, магическая помощь, помощь Таро, Таро Уэйта, Кроули">
        <meta name="title" content="Школа Таро - Школа Таро Галины Мировой">
        <meta name="description" content="Школа Таро, обучение Таро, магия, эзотерика, руны, нумерология, гадание, гадания на Таро, магическая помощь, помощь Таро, Таро Уэйта, Кроули">
        <title><?= Yii::app()->name;?></title>
        <link rel="shortcut icon" href="/img/icon.png" type="image/png">
        <style type="text/css">.cf-hidden { display: none; } .cf-invisible { visibility: hidden; }</style>
    </head>

    <body>

        <div id="wrapper">
            <div class="wrapper-inner">
                <div id="header-row">
                    <div class="row-container">
                        <div class="container">
                            <header>
                                <div class="row">
                                    <div id="logo" class="span4">
                                        <a href="/" >
                                            <span style="font-family: Runic_top; font-size: 48pt;">Школа Таро</span>
                                            </br>
                                            <span style="font-family: victoriana; font-size: 32pt;">Галины Мировой</span>
                                        </a>
                                    </div>
                                    <div class="span4">
                                        <img src="/img/taro.png" alt="Карты таро">
                                    </div>
                                    <div class="moduletable header_text  span4">
                                        <div class="mod-custom mod-custom__header_text">
                                            <div class="left"><span>+7 (908) 803 86 37</span></br><span>Омск</span></div>
                                            <div class="right"><?= BsHtml::link('mir1609@mail.ru', 'mailto:mir1609@mail.ru', array('target'=>'_blank')) ?></div>
                                        </div>
                                        <script type="text/javascript">
                                            (function() {
                                            if (window.pluso)if (typeof window.pluso.start == "function") return;
                                            if (window.ifpluso==undefined) { window.ifpluso = 1;
                                              var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                              s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                              s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                                              var h=d[g]('body')[0];
                                              h.appendChild(s);
                                            }})();
                                        </script>
                                        <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
                                    </div>
                                </div>
                            </header>
                        </div>
                    </div>
                </div>
                
                <div id="navigation-row">
                    <div class="row-container">
                        <div class="container">
                            <div class="row">
                                <nav>
                                    <div class="moduletable navigation  span12">
                                        <?php
                                            $this->widget('zii.widgets.CMenu', array(
                                                'items'=>$result/*array(
                                                    array('label'=>'Главная', 'url'=>array('site/index')),
                                                    array('label'=>'Пункт 1', 'url'=>'#', 'items'=>array(
                                                        array('label'=>'Test 1', 'url'=>'#'),
                                                        array('label'=>'Test 2', 'url'=>'#'),
                                                    )),
                                                    array('label'=>'Пункт 2', 'url'=>'#'),
                                                )*/,
                                                'htmlOptions'=>array(
                                                    'class'=>'sf-menu sf-js-enabled',
                                                ),
                                                'activateParents'=>true,
                                            ));
                                        ?>
                                    </div>
                                    
                                    <!--<div class="moduletable search-block  span4">
                                        <div class="mod-search mod-search__search-block">
                                            <form action="/site/search" method="post" class="navbar-form">
                                                <label for="searchword" class="element-invisible">Search...</label> 
                                                <input id="searchword" name="query" maxlength="20" class="inputbox mod-search_searchword" type="text" size="20" value=" " onblur="if (this.value=='') this.value=' ';" onfocus="if (this.value==' ') this.value='';"> 
                                                <button class="btn btn-primary" onclick="this.form.submit();"> </button> 
                                            </form>
                                        </div>
                                    </div> -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="content-row">
                    <div class="row-container">
                        <div class="container">
                            <div class="content-inner row">
                                <div id="component" class="span12">
                                    <div id="system-message-container">
                                        <div id="system-message">
                                        </div>
                                    </div>
                                    <div class="page-category page-category__">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div id="content">
                <?= $content ?>
            </div>
        </div>
        
        <div id="feature-row">
            <div class="row-container">
                <div class="container">
                    <div class="row">
                        <div class="moduletable welcome  span12">
                            <div class="mod-article-single mod-article-single__welcome">
                                <div class="item__module">
                                    <!-- Yandex.Metrika informer -->
                                    <a href="https://metrika.yandex.ru/stat/?id=28184046&amp;from=informer"
                                    target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/28184046/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                    style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:28184046,lang:'ru'});return false}catch(e){}"/></a>
                                    <!-- /Yandex.Metrika informer -->
                                    <div class="mir-it">
                                        <a href="http://www.mir-it.info/" target="_blank"><img src="/img/logo_footer.png" alt="МИР-ИТ"></a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter28184046 = new Ya.Metrika({id:28184046});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/28184046" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        
    </body>
</html>
